package main

import (
	"context"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/drive/v3"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func main() {
	client, err := getDriveClient()
	if isError(err) {
		return
	}

	var fileToUpload string
	var destinationPath string
	var directoryFiles string
	flag.StringVar(&fileToUpload, "f", "file.zip", "a file name")
	flag.StringVar(&directoryFiles, "dir", "", "directory of files to upload")
	flag.StringVar(&destinationPath, "des", "", "destination folder in drive")
	flag.Parse()

	fmt.Println("file : ", fileToUpload)
	fmt.Println("destination : ", destinationPath)
	fmt.Println("base : ", filepath.Base(destinationPath))

	srv, err := drive.New(client)
	if isError(err) {
		return
	}

	var folderIDList []string
	//parentId := getOrCreateFolder(srv, filepath.Dir(destinationPath))
	//folderIDList = append(folderIDList, parentId)
	folderIDList, err = getOrCreateSubFolderV2(srv, destinationPath)
	if err != nil {
		return
	}

	if len(folderIDList) <= 0 {
		fmt.Println("Folder list id size ", len(folderIDList))
		return
	}

	files := make([]string, 0)
	if directoryFiles == "" {
		if destinationPath == "" {
			destinationPath = fileToUpload
		}
		files = append(files, destinationPath)
	} else {
		list, err := ioutil.ReadDir(directoryFiles)
		if err != nil {
			log.Fatal(err)
		}
		for index, f := range list {
			fmt.Println(index+1, ". ", f.Name())
			if !IsDirectory(f.Name()) {
				files = append(files, fmt.Sprintf("%s/%s", directoryFiles, f.Name()))
			} else {
				fmt.Println("-> is directory..")
			}
		}

		if len(files) == 0 {
			fmt.Println(directoryFiles, "Has no files")
			return
		}
	}

	countUploaded := 0
	for index, file := range files {
		f, err := os.Open(file)
		if isError(err) {
			log.Fatalf("error - %v", err)
			return
		}

		driveFile, err := srv.Files.Create(&drive.File{Name: filepath.Base(file), Parents: folderIDList}).Media(f).Do()
		if err != nil {
			fmt.Printf("Unable to create file: %v", err)
			_ = f.Close()
			return
		} else {
			_ = f.Close()
		}

		countUploaded++
		fmt.Println(index+1, ".", driveFile.Name, "countUploaded...   ", len(files)-countUploaded, "left")
		if index%10 == 0 {
			fmt.Println("sleep!!!")
			time.Sleep(2 * time.Second)
		}
	}

	fmt.Println("---> file countUploaded")
}

func IsDirectory(path string) bool {
	fileInfo, err := os.Stat(path)
	if err != nil {
		return false
	}
	return fileInfo.IsDir()
}

func isError(err error) bool {
	if err != nil {
		fmt.Println("Error - ", err)
		return true
	}
	return false
}

func getOrCreateSubFolderV2(d *drive.Service, path string) ([]string, error) {
	destinationFolder := filepath.Dir(path)
	paths := strings.Split(destinationFolder, "/")
	folderId := "root"
	idParents := make([]string, 0)
	if destinationFolder == "" || destinationFolder == "." {
		return []string{folderId}, nil
	}

	for i := 0; i < len(paths); i++ {
		path := paths[i]

		q := fmt.Sprintf("name='%s' and mimeType='application/vnd.google-apps.folder' and trashed=false", path)
		//for _,idParent := range idParents {
		//	q += fmt.Sprintf(" and '%s' in parents", idParent)
		//}

		if folderId != "" {
			q += fmt.Sprintf(" and '%s' in parents", folderId)
		}

		r, err := d.Files.List().Q(q).PageSize(1).Do()
		if err != nil {
			fmt.Printf("Unable to retrieve foldername by query '%s'. \n%v \n", q, err)
			return nil, err
		}

		if len(r.Files) > 0 { //check if folder exist on google drive
			folderId = r.Files[0].Id
			idParents = append(idParents, folderId)
		} else {
			// no folder found create new
			f := &drive.File{Name: path, Description: "Auto Create by gdrive-upload", MimeType: "application/vnd.google-apps.folder", Parents: []string{folderId}}
			r, err := d.Files.Create(f).Do()
			if err != nil {
				fmt.Printf("An error occurred when create folder: %v\n", err)
				return nil, errors.New(fmt.Sprintf("An error occurred when create folder: %v\n", err))

			}
			folderId = r.Id
			idParents = append(idParents, folderId)
		}
	}

	return []string{folderId}, nil
}

func getDriveClient() (*http.Client, error) {
	b, err := ioutil.ReadFile("gdrive_client_secret.json")
	if isError(err) {
		return nil, err
	}

	config, err := google.ConfigFromJSON(b, drive.DriveScope)
	if isError(err) {
		return nil, err
	}

	client := getClient(config)

	return client, nil
}

// Retrieve a token, saves the token, then returns the generated client.
func getClient(config *oauth2.Config) *http.Client {
	// The file token.json stores the user's access and refresh tokens, and is
	// created automatically when the authorization flow completes for the first
	// time.
	tokFile := "gdrive_token.json"
	tok, err := tokenFromFile(tokFile)
	if err != nil {
		tok = getTokenFromWeb(config)
		saveToken(tokFile, tok)
	}
	return config.Client(context.Background(), tok)
}

func tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	tok := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}

func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		//log.Fatalf("Unable to read authorization code %v", err)
	}

	tok, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		//log.Fatalf("Unable to retrieve token from web %v", err)
	}
	return tok
}

func saveToken(path string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", path)
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		//log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	err = json.NewEncoder(f).Encode(token)
	isError(err)
}
