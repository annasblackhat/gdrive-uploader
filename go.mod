module gitlab.com/annasblackhat/gdrive-uploader

go 1.13

require (
	golang.org/x/oauth2 v0.0.0-20191202225959-858c2ad4c8b6 // indirect
	google.golang.org/api v0.15.0 // indirect
)
